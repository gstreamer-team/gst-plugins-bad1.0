#!/usr/bin/make -f

include /usr/share/dpkg/default.mk
include /usr/share/dpkg/pkg-info.mk

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_LDFLAGS_MAINT_APPEND = -Wl,-O1 -Wl,-z,defs
export HOME=$(CURDIR)/fake-home

# upstream version
gst_version=$(shell echo $(DEB_VERSION) | cut -d '-' -f 1)

built_binaries := $(shell dh_listpackages)

# Let's decide the package name and url depending on the distribution
DISTRO = "$(shell dpkg-vendor --query vendor)"

GST_PACKAGE_NAME := "GStreamer Bad Plugins (unknown Debian derivative)"
GST_PACKAGE_ORIGIN="https://tracker.debian.org/pkg/gst-plugins-bad1.0"

ifeq ($(DISTRO),"Debian")
GST_PACKAGE_NAME := "GStreamer Bad Plugins (Debian)"
GST_PACKAGE_ORIGIN="https://tracker.debian.org/pkg/gst-plugins-bad1.0"
endif

ifeq ($(DISTRO),"Ubuntu")
GST_PACKAGE_NAME := "GStreamer Bad Plugins (Ubuntu)"
GST_PACKAGE_ORIGIN="https://launchpad.net/ubuntu/+source/gst-plugins-bad1.0"
endif

conf_flags = -Dpackage-name=$(GST_PACKAGE_NAME) -Dpackage-origin=$(GST_PACKAGE_ORIGIN)
conf_flags += -Dauto_features=enabled -Ddoc=disabled -Dgpl=enabled
# Not available in Debian yet
conf_flags += -Dfaac=disabled -Dgs=disabled -Diqa=disabled -Disac=disabled -Dmagicleap=disabled
conf_flags += -Dmicrodns=disabled -Dopensles=disabled -Dsvthevcenc=disabled -Dtinyalsa=disabled
# Unmaintained library
conf_flags += -Dmsdk=disabled
# Only available on Windows
conf_flags += -Damfcodec=disabled -Ddirectshow=disabled -Dwasapi=disabled -Dwasapi2=disabled
# In non-free currently
conf_flags += -Dfdkaac=disabled
# DirectFB is orphaned since 2018 (see #1083040)
conf_flags += -Ddirectfb=disabled

ifeq (,$(filter $(DEB_HOST_ARCH),amd64 arm64 armel armhf i386 mips64el ppc64el riscv64 s390x alpha hppa hurd-i386 m68k powerpc ppc64 sparc64))
conf_flags += -Dzxing=disabled
endif

ifeq (,$(filter $(DEB_HOST_ARCH),amd64 arm64 armhf riscv64))
conf_flags += -Donnx=disabled
endif

ifeq ($(filter gstreamer1.0-opencv,$(built_binaries)),)
conf_flags += -Dopencv=disabled
endif

ifeq ($(filter gstreamer1.0-wpe,$(built_binaries)),)
conf_flags += -Dwpe=disabled
endif

ifneq ($(DEB_HOST_ARCH_ENDIAN),little)
conf_flags += -Dldac=disabled
endif

ifneq ($(DEB_HOST_ARCH),amd64)
conf_flags += -Dqsv=disabled
endif

ifneq ($(DEB_HOST_ARCH_OS),linux)
conf_flags += -Dbluez=disabled -Ddc1394=disabled -Dfbdev=disabled -Dsbc=disabled
conf_flags += -Dva=disabled -Dv4l2codecs=disabled -Dwpe=disabled -Duvch264=disabled
conf_flags += -Dvulkan=disabled -Dwayland=disabled -Ddvb=disabled
conf_flags += -Dopenni2=disabled -Dsctp=disabled -Dwebrtc=disabled
endif

ifeq ($(DEB_HOST_ARCH_OS),hurd)
conf_flags += -Dresindvd=disabled -Dnvcodec=disabled -Dgl=disabled -Dudev=disabled
conf_flags += -Duvcgadget=disabled -Davtp=disabled
conf_flags += -Dwebrtcdsp=disabled
endif

# bookworm backport flags
conf_flags += -Daja=disabled -Dqt6d3d11=disabled

%:
	dh $@

override_dh_auto_configure:
	dh_auto_configure -- $(conf_flags)

execute_before_dh_auto_clean:
	rm -rf $(CURDIR)/fake-home

# Only make the tests fail for key architectures
override_dh_auto_test:
ifneq (,$(filter $(DEB_HOST_ARCH), amd64 arm64))
	xvfb-run dh_auto_test
else
	xvfb-run dh_auto_test || true
endif

override_dh_makeshlibs:
	dh_makeshlibs -plibgstreamer-plugins-bad1.0-0 -X "/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0" -V 'libgstreamer-plugins-bad1.0-0 (>= $(gst_version))' -- -c4
	dh_makeshlibs -plibgstreamer-opencv1.0-0 -X "/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0" -V 'libgstreamer-opencv1.0-0 (>= $(gst_version))' -- -c4

execute_after_dh_install:
ifeq ($(DEB_HOST_ARCH_OS),linux)
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstdvb.so
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstfbdevsink.so
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstdc1394.so
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstdecklink.so
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstuvch264.so
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstbluez.so
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstsbc.so
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstwaylandsink.so
	dh_install -plibgstreamer-plugins-bad1.0-0 debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libgstwayland-1.0.so.* usr/lib/$(DEB_HOST_MULTIARCH)
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstkms.so
	dh_install -plibgstreamer-plugins-bad1.0-0 debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libgstcuda-1.0.so.* usr/lib/$(DEB_HOST_MULTIARCH)
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstvulkan.so
	dh_install -plibgstreamer-plugins-bad1.0-0 debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libgstvulkan-1.0.so.* usr/lib/$(DEB_HOST_MULTIARCH)
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstva.so
	dh_install -plibgstreamer-plugins-bad1.0-0 debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libgstva-1.0.so.* usr/lib/$(DEB_HOST_MULTIARCH)
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstv4l2codecs.so
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstopenni2.so
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstsctp.so
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstwebrtc.so
	dh_install -plibgstreamer-plugins-bad1.0-0 debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libgstwebrtcnice-1.0.so.* usr/lib/$(DEB_HOST_MULTIARCH)
ifeq ($(DEB_HOST_ARCH),amd64)
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstqsv.so
endif
endif

ifneq ($(DEB_HOST_ARCH_OS),hurd)
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstresindvd.so
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstnvcodec.so
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstavtp.so
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstgtkwayland.so
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstuvcgadget.so
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstwebrtcdsp.so
endif

ifneq (,$(filter $(DEB_HOST_ARCH),amd64 arm64 armel armhf i386 mips64el ppc64el riscv64 s390x alpha hppa hurd-i386 m68k powerpc ppc64 sparc64))
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstzxing.so
endif

ifneq (,$(filter $(DEB_HOST_ARCH),amd64 arm64 armhf riscv64))
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstonnx.so
endif

ifeq ($(DEB_HOST_ARCH_ENDIAN),little)
	dh_install -pgstreamer1.0-plugins-bad debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/gstreamer-1.0/libgstldac.so
endif

	mkdir -p $(CURDIR)/fake-home
	HOME=$(CURDIR)/fake-home \
	LD_LIBRARY_PATH=debian/libgstreamer-plugins-bad1.0-0/usr/lib/$(DEB_HOST_MULTIARCH):debian/libgstreamer-opencv1.0-0/usr/lib/$(DEB_HOST_MULTIARCH):$(LD_LIBRARY_PATH) \
	    dh_gstscancodecs
	rm -rf $(CURDIR)/fake-home
